<?php
include "lib/config.php"; //App config
include "templates/head.php"; //Head part of HTML (title, meta and link tags)
?>

<body>
<?php
    include "templates/scripts.php"; //Javascript scripts

    include "templates/header/header.php"; //Header part of the site
    include "templates/body/body.php"; //Content part
    include "templates/footer/footer.php"; //Footer of the shop

?>

</body>
</html>

<!-- 
<script type="text/javascript">startLoading();</script>
	<script type="text/javascript">
		$(window).on("load", stopLoading());
	</script> -->