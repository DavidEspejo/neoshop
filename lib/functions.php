<?php
/**
 * Perform a GET request to the API endpoint
 *
 * @param  string $method with the url to to the request
 * @param  boolean $throwsError defines if this method throws errors
 *
 * @return array  with the result
 */

function get($method, $throwsError = true)
{
    /**
     * TODO: Desarrolla un método para obtener los resultados de los ficheros JSON. Preferiblemente utilizando cURL.
     */
    //API Endpoint
    $url = ENDPOINT_URL.$method.".json";

    //Create cURL statement
    $ch = curl_init($url);

    /*---------cURL config---------*/
    //Set content to json
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    //If it fails, curl_error() will return the error msg
    curl_setopt($ch, CURLOPT_FAILONERROR, true);
    //config to return the response
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    /*-----------------------------*/
    
    //execute the request
    $result = curl_exec($ch);

    if (curl_error($ch)) { //Check if there was any error
        $error = curl_error($ch); //Save the error msg in a variable
    }
    
    //close cURL
    curl_close($ch);
    //If there was an error this will be printed
    if($throwsError && isset($error)){
        printf("Error in the request: %s", $error);
        return false;
    }
    return json_decode($result); //convert the result of the request to php object and return it
}
