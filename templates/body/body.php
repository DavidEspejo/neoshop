<div class="wrapper">
    <div class="slider">
        <!-- TODO: Desarrollar el marcado para el slider -->
        <?php include "slider/slider.php"; ?>
    </div><!-- .slider -->
    <div class="products">
        <!-- TODO: Desarrollar el marcado para los productos -->
        <h1 class="products__title">PRODUCTOS</h1>
        <script>loadProducts();</script>
    </div><!-- .products -->
    <div class="load">
        <!-- TODO: BONUS. Desarrollar la funcionalidad para que el botón cargue más productos -->
        <div class="loading_icon"></div>
        <span class="load__button button">Cargar más productos</span>
    </div><!-- .load -->
</div><!-- .wrapper -->