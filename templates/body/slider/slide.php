<?php
  class Slide {
    
    public function __construct($slide){
      $this->id = $slide->id;
      $this->title = $slide->title;

      $this->button = (object) [
        "text" => $slide->button_text,
        "link" => $slide->button_link
      ];

      $this->bg_image = $slide->bg_image;
    }

    public function render() {
      echo "
        <div class='slide' id='slide".$this->id."'>
          <div class='slide__image' style='background-image: url(\"".$this->bg_image."\");'></div>
          
          <div class='slide__content'>
              <div class='slide__title'>
                  ".$this->title."
              </div>
              <div class='slide__button'>
                  <button href='".$this->button->link."' class='button'>
                      ".$this->button->text."
                  </button>
              </div>
          </div>
          
        </div>
      ";
    }
  }

?>