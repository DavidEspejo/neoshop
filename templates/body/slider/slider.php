<?php
    require_once "slide.php";
    $dataSlides = get("slides")->data; //Get the slides data from the API

    function createSlide($slide) {
        $slideObj = new Slide($slide); //Create one slide object
        $slideObj -> render(); //Render slide
    }
?>

<div class="slider__slides" id="mainSlider"> 
    <div class='arrow arrow-left' id="prevSlide"><i class='fas fa-chevron-left'></i></div>
    <div class='arrow arrow-right' id="nextSlide"><i class='fas fa-chevron-right'></i></div>  
    <?php
        foreach ($dataSlides as $slide) { //for each dataSlide create one slide
            createSlide($slide);
        }
    ?>
</div>