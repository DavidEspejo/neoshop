    <div class="scripts">
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script>window.jQuery || document.write('<script src="./js/vendor/jquery/jquery-3.4.1.min.js">x3C/script>')</script>

        <!-- FONT AWESOME -->
        <script src="https://kit.fontawesome.com/2f612e99b2.js"></script>
        
        <!-- AOS JS (Sroll animation) -->
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        <!-- Init AOS library (on loaded content) -->
        <script>$(document).ready(function() {setTimeout(function(){AOS.init();},100);});</script> 


        <script src="./js/const.js" ></script>
        <script src="./js/header.js"></script>
        <script src="./js/body/slider.js"></script>
        <script src="./js/body/products.js"></script>

        
    </div><!-- .scripts -->
