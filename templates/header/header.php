
<header class="header">
    <div class="header__logo">
        <img src="./resources/logo.png" alt="Neoshop" title="Neoshop" />
    </div><!-- .header__logo -->
    <div class="header__menu menu" id="menu__button">
        <div class="menu__dash"></div>
        <div class="menu__dash"></div>
        <div class="menu__dash"></div>
    </div>
    <div class="header__shop">
        <div class="shop">
            <a href="#" class="shop__button">
                <span class="shop__icon"><i class="fas fa-shopping-cart"></i></i></span>
                <span class="shop__text">Carrito</span>
            </a><!-- .shop__button -->
            <a href="#" class="shop__button">
                <span class="shop__icon"><i class="fas fa-user"></i></span>
                <span class="shop__text">Mi perfil</span>
            </a><!-- .shop__button -->
            <a href="#" class="shop__button">
                <span class="shop__icon"><i class="fas fa-box-open"></i></i></span>
                <span class="shop__text">Mis pedidos</span>
            </a><!-- .shop__button -->
        </div><!-- .header__shop -->
    <div>
</header>