<?php
  include_once "../lib/config.php";

  $method = $_SERVER['REQUEST_METHOD'];
  switch ($method) {
    case "GET":
      getProducts();
      break;
    default:
      error("BAD_REQUEST", 400);
      break;
  }

  function getProducts(){
    $dataProducts = get("products", false); //call to get function in config/functions

    if($dataProducts) {
      echo json_encode($dataProducts);
    } else {
      error("ERROR_JSON_NOT_FOUND", 404);
    }
  }

  function error($msg, $code) {
    $error = (object) [
      "message" => $msg,
      "url" => "/api/requestProduts.php",
      "code" => $code,
      "data" =>  []
    ];

    die(json_encode($error));
  }