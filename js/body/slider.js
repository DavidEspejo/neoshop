/* Class that handle Slider animations and iterations */
class animatedSlider {
  defaultClassName = ".slide"; //Name of the slides class

  constructor(idSlider) {
    this.idSlider = `#${idSlider}`;
    this.className = `${this.idSlider} ${this.defaultClassName}`;
    this.currentSlide = 0; //Counter for the current slide
    this.numSlides = $(this.className).length;
    this.hideAll(); //Hides all slides to preserve grid layout
    $(this.className).eq(this.currentSlide).fadeIn(); //Shows the first slide
  }
  /* Functions that handles the slider movement */ 
  nextSlide = () => { 
    this.changeSlide(1);
  }
  prevSlide = () => {
    this.changeSlide(-1);
  }

  /* Function that manages the animation */ 
  changeSlide = (offSet) => {
    this.fadeOut(this.currentSlide);
    this.currentSlide += offSet;
    if(this.currentSlide >= this.numSlides) { //If the slides reach the end or the start
      this.currentSlide = 0;
    } else if(this.currentSlide < 0) {
      this.currentSlide = this.numSlides-1;
    }
    this.fadeIn(this.currentSlide);
  }
  /* Animation functions using jquery animations */
  fadeIn = (numSlide) => {
    $(this.className).eq(numSlide).fadeIn();
  }
  fadeOut = (numSlide) => {
    $(this.className).eq(numSlide).fadeOut();
  }

  /* Hides all slides */
  hideAll = () => {
    $(this.className).each(function() {$(this).hide();})
  } 
}

$(document).ready(function() {
  var slider = new animatedSlider("mainSlider"); //Create one animated Slider
  autoAnimate(slider); //auto animate given slider

  $("#nextSlide").click(function() { slider.nextSlide(); }); //Goes to next slide when btn is clicked
  $("#prevSlide").click(function() { slider.prevSlide(); }); //Goes to previous slide when btn is clicked
});

/*
  Function that automatically goes to nextSlide of given slider
  if user is not hover it
*/
function autoAnimate(mainSlider) {
  var animate = true;
  $(mainSlider.idSlider).mouseenter(() => {animate = false;})
  $(mainSlider.idSlider).mouseleave(() => {animate = true;})  
  setInterval(() => {if(animate) mainSlider.nextSlide();}, 3000);
}