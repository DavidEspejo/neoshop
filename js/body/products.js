$(document).ready(function(){
  $(".load__button").click(function(){
    $(this).hide(); //Hide load button
    $(".loading_icon").fadeIn(); //Show loading icon
    setTimeout(function() { //Delay to load products
      loadProducts(); //Load the products when load button is clicked
      setTimeout(function(){ //Wait everything to render before refresh animations
        AOS.refreshHard();
        $(".loading_icon").fadeOut(); //Hide loading icon 
        $(".load__button").show(); //Show load button
      }, 400); //Hard refresh of AOS library to update new scroll animations
    }, 1000);
    
  });
});

/*
  This function gets the data from the API by doing an ajax request.
  It renders directly the data in the document.
*/
function loadProducts() {
  const container = ".products";
  
  let request = $.ajax({
    url: `${API_ENDPOINT_URL}requestProducts.php`,
    method: "GET",
    dataType: "json"
  });
   
  request.done(function( data ) {
    if(data.code != 200){ //If the request is not successful outputs an error
      console.error(`ERROR (CODE ${data.code}): ${data.message}`);
    } else {
      let products = renderProducts(data.data);
      let CTAs = renderCTAs();
      let html = `
        <div class="product_collection">
          ${products + CTAs}
        </div>
      `;
      $(container).append(html);

    }
  });

  //This function handles errors in the ajax request
  request.fail(function( jqXHR, textStatus, errorThrown ) { 
    console.error(`ERROR (CODE ${jqXHR.status}): ${textStatus}`);
  });
}

//Defines the html structure of the CTAs
function renderCTAs() {
  const html = `
    <div class="cta_image" style="background-image: url('resources/cta/cta1.jpg');" data-aos="fade-left" data-aos-duration="800"></div>
    <div class="cta_image" style="background-image: url('resources/cta/cta2.jpg');" data-aos="fade-right" data-aos-duration="800"></div>

  `;
  return html;
}

//It creates and defines the html structure of the products of given data
function renderProducts(data) {
  var htmlProducts = "";
  data.map(product => {
    let productObj = new Product(product);
    htmlProducts += productObj.render();
  });
  return htmlProducts;
}

//This class defines a individual Product
class Product {
  constructor(product){
    this.id = product.id;
    this.name = product.name;
    this.price = product.price;
    this.image = product.image;
    this.button = { 
      text: product.button_text,
      link: product.button_link
    };
  }
  //html structure of the product
  render = () => {
    const html = `
      <div class="product" data-product-id="${this.id}" data-aos="fade-up" data-aos-offset="100"
      data-aos-duration="500">
        <div class="product__image" style="background-image: url('${this.image}')"></div>

        <div class="product__content">
          <div class="product__name">
            ${this.name}
          </div>
          <div class="product__price">
            ${this.price}
          </div>
          <div class="product__button">
            <button class="button" href="${this.button.link}">
              ${this.button.text}
            </button>
          </div>
        </div>
      </div>
    `;
    return html;
  }
}